import React, { Component } from 'react';
import './App.scss';
import { BrowserRouter as Router, Switch, Route, Link, withRouter} from 'react-router-dom';
import Car from '../Car/Car';
import Fixtures from '../Fixtures/Fixtures';

 class App extends Component {
  render() {
    return (
      <main>
      <div>Здесь вы найдете информацию - топ 5 лучшие машины - светильники</div>
      <Router>
        <nav>
      <div>
            <Link to="/">Выберите категорию просмотра<span>😉</span> </Link>
            <Link class="border" to="/Car">Машины </Link>
            <Link class="border" to="/Fixtures"> Люстры</Link>
        <hr />

        <Switch>
          <Route exact path="/">
            Ничего не выбрано!
            </Route>
          <Route path="/Car" className='imgCar'>
            <Car />
          </Route>
          <Route path="/Fixtures">
            <Fixtures />
          </Route>
        </Switch>
      </div>
      </nav>
    </Router>
      </main>
    );
  }
}

export default withRouter(App);