import React, { Component } from 'react';

const withSecretToLife = (WrappedComponent) => {
  class HOC extends Component {
    render() {
      return <WrappedComponent {...this.props} secretToLife={""} />;
    }
  }
  return HOC;
};

export default withSecretToLife;
